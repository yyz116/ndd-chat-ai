﻿//
// Created by yangyongzhen on 2023/2/14.
//

// You may need to build the project (run Qt uic code generator) to get "ui_DockTitleWidget.h" resolved
#pragma execution_character_set("utf-8")

#include "docktitlewidget.h"
#include "ui_DockTitleWidget.h"
#include <QMessageBox>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonParseError>
#include <QKeyEvent>

DockTitleWidget::DockTitleWidget(QWidget *parent) : QWidget(parent), ui(new Ui::DockTitleWidget) {
    ui->setupUi(this);
//    ui->te_recv->setStyleSheet(
//            "QTextEdit{padding-top:2px;background:#f7f7f7;border-radius:5px;font-size:12px;color:#292421;"
//            "font-family:Microsoft YaHei;padding-left:5px;padding-right:5px;}");//无边框
//    ui->te_send->setStyleSheet(
//            "QTextEdit{padding-top:2px;background:#f7f7f7;border-radius:5px;font-size:12px;color:#292421;"
//            "font-family:Microsoft YaHei;padding-left:5px;padding-right:5px;}");//无边框

    connect(ui->pb_send, SIGNAL(clicked(bool)), this, SLOT(slotBtn_SendClick()));
    connect(ui->te_recv, SIGNAL(textChanged()), this, SLOT(slotEdtReceiveTextChanged()));
    connect(this,SIGNAL(sigAppendText(QString)),this,SLOT(slotAppendText(QString)));

    ui->te_send->installEventFilter(this);
    m_http = new QNetworkAccessManager();
}

DockTitleWidget::~DockTitleWidget() {
    delete ui;
    delete m_http;
    if (m_resp != nullptr) {
        delete m_resp;
    }
}

void DockTitleWidget::slotBtn_SendClick() {
    //设置头信息
    QNetworkRequest m_url;
    //m_url.setUrl(QUrl("https://pmp.eloam.net/api/ota/findFadVersion"));
    m_url.setUrl(QUrl(serverUrl));
    m_url.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");

    QSslConfiguration m_sslConfig = QSslConfiguration::defaultConfiguration();
    m_sslConfig.setPeerVerifyMode(QSslSocket::VerifyNone);
    m_sslConfig.setProtocol(QSsl::TlsV1_2);
    m_url.setSslConfiguration(m_sslConfig);

    //char cByte[1024] = "{\"as\":\"123456\", \"ks\": \"123456\", \"productCode\": \"HSPS\", \"version\": \"V1.2.3\"}";
    //char cByte[1024] = "{\"as\":\"123456\", \"ks\": \"123456\", \"productCode\": \"HSPS\", \"version\": \"V1.2.3\"}";
    auto send = QString(u8"{\"user\":\"%1\",\"msg\":\"%2\",\"key\":\"%3\",\"code\":\"%4\"}").arg(user).arg(ui->te_send->toPlainText()).arg(apiKey).arg(code);
    QByteArray bate = send.toUtf8();
    //QByteArray bate(send);
    QString fromName = QString(u8"<br/>[我]"+QDateTime::currentDateTime().toString(" [yy-MM-dd hh:mm:ss]:"));
    fromName = QString("<font color = #292421>%1</font>").arg(fromName);//必须用br作为换行符
    ui->te_recv->append(fromName);
    //ui->te_recv->setAlignment(Qt::AlignRight);
    ui->te_recv->append(ui->te_send->toPlainText());
    m_resp = m_http->post(m_url, bate);
    connect(m_resp, &QNetworkReply::finished, this, &DockTitleWidget::slotNet_Received);

}

void DockTitleWidget::slotNet_Received() {
    QString fromName = QString(u8"<br/>[Chat-AI]")+ QDateTime::currentDateTime().toString(" [yy-MM-dd hh:mm:ss]:");
    fromName = QString("<font color = #3D59AB>%1</font>").arg(fromName);//必须用br作为换行符
    ui->te_recv->append(fromName);
    if (m_resp->error() == QNetworkReply::NoError) {
        QString strReceive = m_resp->readAll();      // 自行解析接口返回数据
        //ui->te_recv->append(ba);
        //QMessageBox::warning(this, "123", ba);
        QJsonParseError json_error;
        QJsonDocument doc = QJsonDocument::fromJson(strReceive.toUtf8(), &json_error);
        if (!doc.isNull() && json_error.error == QJsonParseError::NoError) {
            if (doc.isObject()) {
                QJsonObject object = doc.object();
                if (object.contains("text")) {  // 包含指定的 key
                    QJsonValue value = object.value("text");
                    if (value.isString()) {
                        ui->te_recv->append(value.toString());
                    }
                }
            }
        } else {
            ui->te_recv->append(strReceive);
        }
        //auto recv = QJsonDocument::fromJson(strReceive);
    } else {
        ui->te_recv->append(m_resp->errorString());
        //QMessageBox::warning(this, "123", m_resp->errorString());
    }
}

void DockTitleWidget::slotEdtReceiveTextChanged() {
    QTextCursor cursor = ui->te_recv->textCursor();
    cursor.movePosition(QTextCursor::End);
    ui->te_recv->setTextCursor(cursor);
}

void DockTitleWidget::slotAppendText(const QString &text) {

}

bool DockTitleWidget::eventFilter(QObject *target, QEvent *event) {
    if(target == ui->te_send)
    {
        if(event->type() == QEvent::KeyPress)//按键按下
        {
            QKeyEvent *evt = static_cast<QKeyEvent *>(event);
            if ((evt->key() == Qt::Key_Return) && (evt->modifiers() ==(Qt::ControlModifier)))
            {
                //qDebug()<<"eventFilter Enter clicked";
                slotBtn_SendClick();
                return true;
            }
        }
    }
    return QObject::eventFilter(target, event);
}

void DockTitleWidget::setApiKey(const QString &apiKey) {
    DockTitleWidget::apiKey = apiKey;
}

void DockTitleWidget::setServerUrl(const QString &serverUrl) {
    DockTitleWidget::serverUrl = serverUrl;
}

void DockTitleWidget::setUser(const QString &user) {
    DockTitleWidget::user = user;
}

void DockTitleWidget::setCode(const QString &code) {
    DockTitleWidget::code = code;
}
