//
// Created by Administrator on 2023/3/16.
//

#ifndef CHATAI_NDD_MYPLUGIN_H
#define CHATAI_NDD_MYPLUGIN_H
#include <QAction>
#include <QObject>
#include <QWidget>
#include <qsciscintilla.h>
#include "mysettings.h"

class QDockWidget;

class NDDMyPlugin  : public QObject{
Q_OBJECT
public:
    explicit NDDMyPlugin(QWidget *mainWidget, const QString &pluginPath, QsciScintilla *pEdit,
    QObject *parent = nullptr);
    ~NDDMyPlugin() override = default;

    void getViewMenu(QMenu *menu);
    void setScintilla(const std::function<QsciScintilla *()> &cb);

private:
    QWidget *mainWidget_;
    QDockWidget *dockWidget_;

    MySettings *mySettings_;

private:

    std::function<QsciScintilla *()> scintillaCallback_;
};


#endif //HELLOWORLD_NDDMYPLUGIN_H
