//
// Created by Administrator on 2023/3/16.
//

#include "NDDMyPlugin.h"
#include "docktitlewidget.h"
#include <QDockWidget>
#include <QHeaderView>
#include <QMainWindow>
#include <QMenuBar>

NDDMyPlugin::NDDMyPlugin(QWidget *mainWidget, const QString &pluginPath, QsciScintilla *pEdit, QObject *parent)
        : QObject(parent),
          dockWidget_(new QDockWidget("ChatGPT-AI窗口")),
          mySettings_(new MySettings(pluginPath)),
          mainWidget_(mainWidget)
{
    dockWidget_->setFeatures(QDockWidget::DockWidgetFloatable | QDockWidget::DockWidgetClosable|QDockWidget::DockWidgetMovable);

    dockWidget_->setAllowedAreas(Qt::LeftDockWidgetArea);;
    dockWidget_->hide();

    auto dockWidgetTitle = new DockTitleWidget;
    //dockWidget_->setTitleBarWidget(dockWidgetTitle);
    dockWidgetTitle->setApiKey(mySettings_->getPluginSetting().apikey);
    dockWidgetTitle->setServerUrl(mySettings_->getPluginSetting().serverUrl);
    dockWidgetTitle->setUser(mySettings_->getPluginSetting().user);
    dockWidgetTitle->setCode(mySettings_->getPluginSetting().code);
    dockWidget_->setWidget(dockWidgetTitle);
    auto mainWindow = dynamic_cast<QMainWindow *>(mainWidget);
    mainWindow->addDockWidget(Qt::LeftDockWidgetArea, dockWidget_);
}
void NDDMyPlugin::getViewMenu(QMenu *menu)
{
    menu->addAction("Show Chat Window", this, [this] {
        dockWidget_->show();
    });
//    menu->addAction(
//            "快捷方式(Ctrl+F8)", this, [this] {
//                }, Qt::CTRL + Qt::Key_F8);
//    menu->addAction(
//            "Compress Json(Ctrl+F9)", this, [this] {  }, Qt::CTRL + Qt::Key_F9);
    menu->addAction("Settings", this, [this] {
        this->mySettings_->show();
        //做一个简单的转大写的操作
        //QtTestClass* p = new QtTestClass(mainWidget_,scintillaCallback_());
        //主窗口关闭时，子窗口也关闭。避免空指针操作
       // p->setWindowFlag(Qt::Window);
        //p->show();
    });
}

void NDDMyPlugin::setScintilla(const std::function<QsciScintilla *()> &cb)
{
    if(scintillaCallback_== nullptr){
        scintillaCallback_ = cb;
    }
}
