﻿//
// Created by Administrator on 2023/3/23.
//

// You may need to build the project (run Qt uic code generator) to get "ui_MySettings.h" resolved

#include "mysettings.h"
#include "ui_MySettings.h"
#include <QDir>
#include <QSettings>

MySettings::MySettings(const QString &pluginPath, QWidget *parent) :
        QWidget(parent), ui(new Ui::MySettings),configSetting_(nullptr) {
    ui->setupUi(this);

    ui->lb_help->setOpenExternalLinks(true);//设置为true才能打开网页
    ui->lb_help->setText("<a style='color: green; text-decoration: none' href = https://blog.csdn.net/qq8864?type=lately>使用帮助");
    ui->lb_help->setAlignment(Qt::AlignCenter);//文字居中

    auto pluginDllName = pluginPath;

    init(pluginDllName.replace("ndd-chat-ai.dll", ""));

    connect(ui->btn_ok, &QPushButton::clicked, this, &MySettings::slot_BtnOkClicked);
}

MySettings::~MySettings() {
    delete ui;
}

void MySettings::slot_BtnOkClicked() {

    if(!ui->le_key->text().isEmpty()){
        configSetting_->setValue(INI_SERVER_CFG_KEY, ui->le_key->text());
    }
    if(!ui->le_url->text().isEmpty()) {
        configSetting_->setValue(INI_SERVER_CFG_URL, ui->le_url->text());
    }
    if(!ui->le_code->text().isEmpty()) {
        configSetting_->setValue(INI_SERVER_CFG_CODE, ui->le_code->text());
    }

    pluginSetting_.apikey = configSetting_->value(INI_SERVER_CFG_KEY).toString();
    pluginSetting_.serverUrl = configSetting_->value(INI_SERVER_CFG_URL).toString();
    pluginSetting_.code = configSetting_->value(INI_SERVER_CFG_CODE).toString();
    this->hide();
}

void MySettings::init(const QString &pluginPath) {
    QDir pluginDir(pluginPath);
    if (!pluginDir.cd("config"))
    {
        pluginDir.mkdir("config");
        pluginDir.cd("config");
    }

    configSetting_ = new QSettings(pluginDir.absolutePath() + "/ChatServer.ini", QSettings::IniFormat);
    if (!configSetting_->contains(INI_SERVER_CFG_KEY))
    {
        configSetting_->setValue(INI_SERVER_CFG_KEY, "sk-xx");
    }
    if (!configSetting_->contains(INI_SERVER_CFG_URL))
    {
        configSetting_->setValue(INI_SERVER_CFG_URL, "https://chat.yangqq.repl.co/mychat");
    }
    if (!configSetting_->contains(INI_SERVER_CFG_USER))
    {
        configSetting_->setValue(INI_SERVER_CFG_USER, "guest");
    }
    if (!configSetting_->contains(INI_SERVER_CFG_CODE))
    {
        configSetting_->setValue(INI_SERVER_CFG_CODE, "");
    }
    pluginSetting_.apikey = configSetting_->value(INI_SERVER_CFG_KEY).toString();
    pluginSetting_.serverUrl = configSetting_->value(INI_SERVER_CFG_URL).toString();
    pluginSetting_.user = configSetting_->value(INI_SERVER_CFG_USER).toString();
    pluginSetting_.code = configSetting_->value(INI_SERVER_CFG_CODE).toString();
}

