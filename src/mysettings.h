//
// Created by Administrator on 2023/3/23.
//

#ifndef CHATAI_MYSETTINGS_H
#define CHATAI_MYSETTINGS_H

#include <QWidget>
#define INI_SERVER_CFG_KEY "ChatServer/APIKEY"
#define INI_SERVER_CFG_URL "ChatServer/SERVER"
#define INI_SERVER_CFG_USER "ChatServer/USER"
#define INI_SERVER_CFG_CODE "ChatServer/CODE"
QT_BEGIN_NAMESPACE
namespace Ui { class MySettings; }
QT_END_NAMESPACE
class QSettings;
struct Setting
{
    QString apikey = "";
    QString serverUrl = "";
    QString user="";
    QString code="";
};

class MySettings : public QWidget {
Q_OBJECT

public:
    explicit MySettings(const QString &pluginPath, QWidget *parent = nullptr);

    ~MySettings() override;

public:
    [[nodiscard]] const Setting &getPluginSetting() const { return pluginSetting_; }

private:
    Ui::MySettings *ui;
    QSettings *configSetting_;
    Setting pluginSetting_;

    void init(const QString &pluginPath);

private slots:
    void slot_BtnOkClicked();
};


#endif //HELLOWORLD_MYSETTINGS_H
