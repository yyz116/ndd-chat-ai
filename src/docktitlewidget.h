﻿//
// Created by yangyongzhen on 2023/3/14.
//

#ifndef NDD_CHAT_AI_DOCKTITLEWIDGET_H
#define NDD_CHAT_AI_DOCKTITLEWIDGET_H

#include <QWidget>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>

QT_BEGIN_NAMESPACE
namespace Ui
{
class DockTitleWidget;
}
QT_END_NAMESPACE

class DockTitleWidget : public QWidget
{
    Q_OBJECT

public:
    explicit DockTitleWidget(QWidget *parent = nullptr);
    ~DockTitleWidget() override;

signals:
    void sigAppendText(const QString& text);

private:
    Ui::DockTitleWidget *ui;

    QNetworkAccessManager* m_http;
    QNetworkReply* m_resp;

    QString apiKey = "";
    QString serverUrl = "";
    QString user = "";
    QString code = "";
public:
    void setCode(const QString &code);

public:
    void setUser(const QString &user);

public:
    void setApiKey(const QString &apiKey);

    void setServerUrl(const QString &serverUrl);

private:

    bool eventFilter(QObject *target, QEvent *event);//事件过滤器

private slots:

    void slotBtn_SendClick();
    void slotNet_Received();

    void slotEdtReceiveTextChanged();
    void slotAppendText(const QString& text);
};

#endif  // NDD_JSON_VIEWER_DOCKTITLEWIDGET_H
