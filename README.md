# NDD-ChatAI

#### 介绍

该项目用于解决 [NDD](https://gitee.com/cxasm/notepad--) 软件的 CHAT-GPT的聊天窗口插件。

不想编译的可以直接找到ndd-chat-ai.dll插件，放置在NDD的plugin目录即可。

由于使用了https访问网络，官方发布版可能没带libssl-1_1-x64.dll和libcrypto-1_1-x64.dll
目录libs文件夹中带的有，也需要拷贝进根目录里。

使用方式分两种：

方式一：可使用自己的open-api的key,服务地址不配置则默认使用作者的后台服务。
若自己有后台的，则可以配置自己的后台地址。

后台使用的接口报文json格式如下：

请求：
post https://chat.yangqq.repl.co/mychat

content-type: application/json

{
	"user": "yang",
	"key": "sk-xxxx",
	"msg": "hello,who are you?",
	"code": "liming-123"
}

应答：

{
	"code": 0,
	"text": "this is response from chat-gpt",
	 "msg":  "成功"
}

方式二：只需配置访问码，联系插件作者获取访问码。

#### 运行示例
![Chat Window1](images/show4.png)
![Chat Window2](images/show1.png)
![Chat Window3](images/show3.png)

#### 编译流程

**前提：**

插件的编译说明请参考[这里](https://gitee.com/cxasm/notepad--/blob/master/%E7%BC%96%E8%AF%91%E8%AF%B4%E6%98%8E.docx)。



该项目仅支持使用 `CMake`编译。

编译很简单，独立编译，只需依赖QT库和qmyedit_qt5.lib

#### CMake 编译

依赖项：libs文件夹中的头文件和lib库(qmyedit_qt5.lib)

* Qt Library

编译参数只需要一个参数：

* `CMAKE_PREFIX_PATH` ：表明 QT 的 SDK 存放位置

示例：

```bash
mkdir build && cd build
cmake .. -DCMAKE_PREFIX_PATH=D:\Qt5.12.11\Qt5.12.11\5.12.11\msvc2015_64\lib\cmake -G Ninja
ninja
```

#### QMake 编译

暂无

<!--将该工程放在 `%ndd_root_path%/src/plugin` 文件夹中，且已经将 `QScint` 已经编译成动态库。使用 `QtCreator` 打开 `%ndd_root_path%/src/plugin/ndd-chat-ai.pro` 文件即可一键编译。-->



#### 安装教程

将该项目生成的动态库拷贝至 `%NPP_INSTALL_PATH%/plugin`，重启 NPP 即可。

#### 使用说明

通过菜单栏的 `Chat Window View` 打开聊天窗口。


#### 参与贡献


#### 特技

